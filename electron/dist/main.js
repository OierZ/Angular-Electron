"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Import necesarios
var electron_1 = require("electron");
// Inicializamos la ventana de Electron
var win;
function createWindow() {
    win = new electron_1.BrowserWindow({ width: 800, height: 600 });
    // win.loadURL(
    //   url.format({
    //     pathname: path.join(__dirname, `/dist/angular-electron/index.html`),
    //     protocol: 'file:',
    //     slashes: true,
    //   })
    // );
    win.loadFile('dist/index.html');
    win.webContents.openDevTools();
    console.log('entra');
    win.on('closed', function () {
        win = null;
    });
}
// Para ver el estado de la app
electron_1.app.on('ready', createWindow);
electron_1.app.on('activate', function () {
    if (win === null) {
        createWindow();
    }
});
//# sourceMappingURL=main.js.map