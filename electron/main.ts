// Import necesarios
import { app, BrowserWindow } from 'electron';

// Inicializamos la ventana de Electron
let win: BrowserWindow;
function createWindow() {
  win = new BrowserWindow({ width: 800, height: 600 });

  win.loadFile('dist/index.html');

  win.webContents.openDevTools();

  win.on('closed', () => {
    win = null;
  });
}
// Para ver el estado de la app
app.on('ready', createWindow);

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});
